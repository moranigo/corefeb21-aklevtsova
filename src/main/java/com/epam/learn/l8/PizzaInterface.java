package com.epam.learn.l8;

public interface PizzaInterface extends Payable,Checker {

    void wash();

    void cook();

    void delivery();

    default void sayHello() {
        System.out.println("hello from interface");
    }

    static void sayHi() {
        System.out.println(" static hi from interface");
    }

}
