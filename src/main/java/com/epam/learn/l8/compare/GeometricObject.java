package com.epam.learn.l8.compare;

public abstract class GeometricObject {

    public abstract double getArea();
}
