package com.epam.learn.l8.compare;

public class RectangleGO extends GeometricObject {
    private double sideA;

    public RectangleGO(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    private double sideB;



    @Override
    public double getArea() {
        return sideA*sideB;
    }
}
