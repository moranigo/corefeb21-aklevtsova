package com.epam.learn.l8.impl;

import com.epam.learn.l8.PizzaInterface;

public class PizzaMSK implements PizzaInterface {
    @Override
    public void wash() {
        System.out.println("Wash");

    }

    @Override
    public void cook() {
        System.out.println("Cooking");
    }

    @Override
    public void delivery() {
        System.out.println("Delivery");

    }

    @Override
    public void pay() {
        System.out.println("Pay");
    }

    @Override
    public void check() {
        System.out.println("Checked");
    }
}
