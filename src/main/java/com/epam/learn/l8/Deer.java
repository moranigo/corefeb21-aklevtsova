package com.epam.learn.l8;

import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

public class Deer {

    public Deer() {
        System.out.print("Deer");
    }
    public Deer(int x) {
        System.out.println("DeerAge");
    }
    private boolean hasHorns() {return false;}

    public static void main(String[] args) {
        Reindeer deer = new Reindeer(5);
        System.out.println(","+deer.hasHorns());

        LocalDate.of(2015, Month.APRIL,1);
        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, Calendar.APRIL,1);
       
    }
}

class Reindeer extends Deer {
    public Reindeer(int age) {
        System.out.print("Reindeer");
    }
    public boolean hasHorns() {return true;}
}
