package com.epam.learn.l8;

import java.util.Arrays;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;
    private int age;

    public static void main(String[] args) {
        Person tom = new Person();
        tom.setAge(10);

        Person bob = new Person();
        bob.setAge(12);
        System.out.println(tom.compareTo(bob));

        Person[] person = {bob,tom};
        Arrays.sort(person);
        for(Person p: person) {
            System.out.println(p.age);
        }
    }

    @Override
    public int compareTo(Person anotherPerson) {
        int anotherPersonAge = anotherPerson.age;
        return this.age - anotherPersonAge;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
