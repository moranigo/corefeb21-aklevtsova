package com.epam.learn.l8;

import java.util.Date;

public class Department implements Cloneable {

    private Integer codeName=0;
    private Date date = new Date();

    @Override
    public Object clone() throws CloneNotSupportedException {
        Department obj;
        obj = (Department) super.clone();
        if(null != this.date) {
            obj.date = (Date) this.date.clone();
        }
        return obj;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Department department = new Department();
        department.codeName = 32;
        department.date = new Date();

        Department department1 = (Department) department.clone();
       department1.date = new Date();

        System.out.println(department1.date+ "   " + department.date);
    }
}
