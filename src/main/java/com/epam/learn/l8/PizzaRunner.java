package com.epam.learn.l8;

import com.epam.learn.l8.impl.PizzaMSK;

public class PizzaRunner {
    private PizzaInterface pizzaInterface;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
        PizzaInterface.sayHi();
    }

    private void getPizzaInfo() {
        pizzaInterface.sayHello();
        pizzaInterface = new PizzaMSK();
        pizzaInterface.wash();
        pizzaInterface.cook();
        pizzaInterface.delivery();
    }
}
