package com.epam.learn.l18;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Main6 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3);
//        list.stream().map(o->o+1).mapToInt(o->o).forEach(System.out::println);
//        list.stream().map(o->o+1).mapToDouble(o->o).forEach(System.out::println);
//        list.stream().map(o->o+1).mapToLong(o->o).forEach(System.out::println);
//
//        IntStream.of(1).mapToObj(o->o+" Cats").forEach(System.out::println);
//        list.stream().flatMapToInt(i->IntStream.range(0,i)).forEach(System.out::println);
//        Stream.of(10,20,30,40).limit(3).forEach(System.out::println);
//        Stream.of(10,20,30,40).skip(3).forEach(System.out::println);
//        Stream.of(1,5,3,4,2).sorted().forEach(System.out::println);
//          Stream.of(1,3,5,2,3,3,4,2).distinct().forEach(System.out::println);
//          Stream.of(1,3,5,2,3,3,4,2)
//                  .peek(o-> System.out.format("before distinct: %d%n", o))
//                  .distinct()
//                  .peek(o-> System.out.format("after distinct: %d%n", o))
//                  .map(o->o*o)
//                  .forEach(o-> System.out.println());
//        Stream.of(1,3,5,2,3,3,4,2).takeWhile(o->o>4).forEach(System.out::println);
//        Stream.of(1,3,5,2,3,3,4,2).dropWhile(o->o>4).forEach(System.out::println);

//        List<Integer> list1 = Stream.of(1,2,3).collect(Collectors.toList());
//        System.out.println(list1);
//
//        String value = Stream.of(1,2,3).map(String::valueOf).collect(Collectors.joining(":","<",">"));
//        System.out.println(Stream.of(1,2,3).noneMatch(o->o==4));
        System.out.println(IntStream.range(1, 9).sum());

    }
}
