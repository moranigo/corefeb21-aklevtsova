package com.epam.learn.l18;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Main2 {
    public static void main(String[] args) {
        IntStream.of(20, 40, 120, 500, 32, 12)
                .filter(x -> x < 100)
                .map(x -> x + 5)
                .limit(1)
                .forEach(System.out::println);


        List<String> list = Arrays.asList("bk", "murzik");
        list.stream().filter(s -> s.length() > 2).forEach(System.out::println);
    }
}
