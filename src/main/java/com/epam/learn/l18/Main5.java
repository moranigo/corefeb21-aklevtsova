package com.epam.learn.l18;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Main5 {
    public static void main(String[] args) {
        IntStream intStream;
        LongStream longStream;
        DoubleStream doubleStream;

        Stream.empty().forEach(System.out::println);
//        Stream.of(1,2,3).forEach(System.out::println);
//        Arrays.asList(1,2,3).stream().forEach(System.out::println);

//        String value= Math.random() > 0.8 ? "I am happy": null;
//        Stream.ofNullable(value).forEach(System.out::println);

//        Stream.generate(()->10).limit(10).forEach(System.out::println);

//        Stream.iterate(2,s->s+3).limit(5).forEach(System.out::println);

        Stream.concat(
                Stream.of(1, 2, 3, 4, 5),
                Stream.of(6, 7, 8, 9, 10)
        ).forEach(System.out::println);


    }
}
