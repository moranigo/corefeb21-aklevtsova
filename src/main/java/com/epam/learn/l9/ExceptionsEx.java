package com.epam.learn.l9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExceptionsEx {

    public static void main(String[] args) {
        System.out.println("Hello!");
        //throw new RuntimeException("I am exception");

        //throw - выброс исключения
        //throws - перекладываю ответственность обработки исключения на кого-то еще
        //try- попытка выполнить какой-то кусок кода, где может быть исключение
        //catch - отлавливаем исключительное событие
        //finally - блок кода который будет выполняться всегда

        try {
            throw new RuntimeException("ooops");
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        finally {
            System.out.println("finally!");
        }

        System.out.println("By!");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // или так. Try with resources s
        try (BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println(bufferedReader2.readLine());
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    void getMoney() throws IOException {
        throw new IOException();
    }

    void getMoney2() throws RuntimeException {
        throw new RuntimeException();
    }

    void getInfo() {
        try {
            getMoney();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getMoney2();
    }
}
