package com.epam.learn.l9.generics;

import com.epam.learn.l9.Doctor;

import java.util.ArrayList;
import java.util.List;

public class Main2<T> {
    T count;
    List<? super T> list = new ArrayList<>();


    public static void main(String[] args) {
        Main2<Number> main2 = new Main2<>();
        Object object = main2.list.get(2);
        main2.count = 10;
        main2.list.add(2);

        main2.asByte("Adsas");
        main2.asByte(2);
        main2.asByte(new Doctor());

    }

    public boolean sameAny(Main2<? extends Number> ob) {
        return true;
    }

    public <u> boolean sameAny2(Main2<u> ob) {
        return true;
    }

    public void getName(List<? extends T> list2) {
        list2 = new ArrayList<>();

        Main2<Number> main2 = new Main2<>();
        System.out.println(list2.contains(2));
        //list2.add(2); - нельзя так, как мы не можем гарантировать, что мы положим правильный лист в объект
    }

    public <E extends Number> byte asByte(E mum) {
        Integer.reverse((Integer)mum);
        return 1;
    }

    public <E> byte asByte(E num) {
        E num2 = (E) new Object();
        asByte(2);
        return 1;
    }

}
