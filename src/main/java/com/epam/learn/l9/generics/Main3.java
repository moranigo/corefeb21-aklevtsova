package com.epam.learn.l9.generics;

public class Main3<T> {

    T type;

    public Main3(T type) {
        this.type = type;
    }

    public static void main(String[] args) {
        Main3<String> main3 = new Main3<>("string");

        main3.getSomething(main3.type);

        Main3<Integer> main31 = new Main3<>(123);
      //  main3.getSomething(main31.type); - неверный тип

        main3.getSomething2(main31.type);

        main3.getSomething3(main3);
        //main3.getSomething3(main31);

        main3.getSomething4(main31);
        main3.getSomething5(main31);

        main3.getE("String");
        main3.getE(12312);

        main3.<String>getE2();
        main3.<Integer>getE2();


    }

    public void getSomething(T t) {
        System.out.println(t.getClass().getName());
    }
    public <T> void getSomething2(T t) {
        System.out.println(t.getClass().getName());
    }

    public void getSomething3(Main3<T> t) {
        System.out.println(t.getClass().getName());
    }

    public void getSomething4(Main3<?> t) {
        System.out.println(t.getClass().getName());
    }
    public <U> void getSomething5(Main3<U> t) {
        System.out.println(t.getClass().getName());
    }

    public <E> E getE(E el){
        System.out.println(el.getClass().getName());
        return el;
    }

    public <E> E getE2(){
        E el = (E) new Object();
        return el;
    }


}
