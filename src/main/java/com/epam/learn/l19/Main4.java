package com.epam.learn.l19;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.TemporalAmount;

public class Main4 {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);

        LocalDate barsikBirthday = LocalDate.of(1999, 4, 15);
        LocalDate barsikBirthday2 = LocalDate.of(1999, Month.APRIL, 15);
        barsikBirthday = barsikBirthday.plusYears(2);
        barsikBirthday = barsikBirthday.minusWeeks(2);

        TemporalAmount amount = Period.ofDays(2);
        barsikBirthday = barsikBirthday.plus(amount);


        barsikBirthday = barsikBirthday.withDayOfMonth(2);
        barsikBirthday = barsikBirthday.withDayOfYear(2);
        barsikBirthday = barsikBirthday.withMonth(2);
        barsikBirthday = barsikBirthday.withYear(2001);

        System.out.println(barsikBirthday);
        System.out.println(barsikBirthday.getDayOfMonth());
        System.out.println(barsikBirthday.getDayOfYear());
        System.out.println(barsikBirthday.getDayOfWeek());
        System.out.println(barsikBirthday.getMonth());
        System.out.println(barsikBirthday.getYear());
        System.out.println(barsikBirthday.getMonthValue());

        //  Period period = barsikBirthday.until();
        System.out.println(barsikBirthday.isLeapYear());

    }
}
