package com.epam.learn.l19.legacyDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Date date = new Date(); //present time
        date.setTime(1);
        System.out.println(date.getTime());

        Date date1 = new Date(0); // Jan 01 1970
        System.out.println(date1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        String dateInString = "31-05-1985 10:20:56";
        Date date3 = null;
        try {
            date3 = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date3);
    }
}
