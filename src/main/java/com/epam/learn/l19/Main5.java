package com.epam.learn.l19;

import javax.sound.midi.Soundbank;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Vector;
import java.util.function.*;

public class Main5 {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.of(10,2);
        localTime.plusHours(1);
        localTime.plusMinutes(2);
        localTime.minusNanos(2);
        localTime.minusSeconds(2);

        long value = localTime.toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        System.out.println(value);
        System.out.println(localTime);

        LocalDateTime localDateTime = LocalDateTime.now();

        ZonedDateTime zonedDateTime = ZonedDateTime.of(2020,2,2,2,
                2,2,2,ZoneId.of("UTC+3"));
        System.out.println(zonedDateTime.getOffset());
        System.out.println(zonedDateTime);

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL);
        String format = formatter.format(zonedDateTime);
        System.out.println(format);

        final Instant now = Instant.now();
        final Date date  = Date.from(now);
        System.out.println(date.toInstant());
        System.out.println(now);

        ZonedDateTime now1 = ZonedDateTime.now();
        GregorianCalendar calendar = GregorianCalendar.from(now1);
        System.out.println(calendar.toZonedDateTime());
        System.out.println(now1);

        LocalDateTime now3 = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(now3);
        System.out.println(timestamp.toLocalDateTime());
        System.out.println(now3);

        LocalDate dateNow = LocalDate.now();
        java.sql.Date date1 = java.sql.Date.valueOf(dateNow);
        System.out.println(dateNow);
        System.out.println(date1.toLocalDate());

        LocalTime nowTime = LocalTime.now();
        Time time = Time.valueOf(nowTime);
        System.out.println(nowTime);
        System.out.println(time.toLocalTime());

    }
}
