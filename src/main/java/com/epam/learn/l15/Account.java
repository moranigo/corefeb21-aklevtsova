package com.epam.learn.l15;

public class Account {
    private int balance;
    private final Object lock = new Object();

    public Account(int balance) {
        this.balance = balance;
    }

//    public synchronized void deposit(int amount) throws InterruptedException {
//        int x = balance + amount;
//        Thread.sleep(1L);
//        balance = x;
//    }


    public void deposit(int amount) throws InterruptedException {
        System.out.println("I'm deposit: " + amount);
        synchronized (this) {
            int x = balance + amount;
            Thread.sleep(1L);
            balance = x;
        }
    }

//    public synchronized void withdrow(int amount) throws InterruptedException {
//        int x = balance - amount;
//        Thread.sleep(1L);
//        balance = x;
//    }

    public void withdrow(int amount) throws InterruptedException {
        System.out.println("I'm withdrow: " + amount);
        synchronized (this) {
            int x = balance - amount;
            Thread.sleep(1L);
            balance = x;
        }
    }

    public int getBalance() {
        return balance;
    }
}
