package com.epam.learn.l15;

public class Main {
    private final Object lock = new Object();
    public static void main(String[] args) {

    }

    void doSomething() {
        System.out.println("I am here");

        synchronized (lock) {

        }
    }
}
