package com.epam.learn.l12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main4 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("barsik");
        list.add("barsik");
        list.add("murzik");

        Iterator<String> iterator = list.iterator();
        while(iterator.hasNext()) {
            String el = iterator.next();
            if(el.equals("barsik")) {
                iterator.remove();
            }
        }

        //Java 8
        list.removeIf(o -> o.equals("barsik"));

        System.out.println(list);
    }
}
