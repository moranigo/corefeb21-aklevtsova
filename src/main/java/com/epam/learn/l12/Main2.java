package com.epam.learn.l12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main2 {
    //General-purpose
    //Special-purpose
    //Concurrent
    //Wrapper
    //Convenience
    //Abstract

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();

        System.out.println("\n\n\nforEach:");
        list.add("Barsik");
        list.add("Murzik");

        for (String value: list) {
            System.out.println(value);
        }

        //Java 8
        System.out.println("\n\n\nforEach Java 8:");
        list.forEach(System.out::println);

        System.out.println("\n\n\nIterator:");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }




    }
}
