package com.epam.learn.l21;

public class Main2 {

    public static void main(String[] args) {
        //Если элементов <10 = o(1);


        //O(n^2)
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.println(i + j);
            }
        }

        //O(n)
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
    }
}
