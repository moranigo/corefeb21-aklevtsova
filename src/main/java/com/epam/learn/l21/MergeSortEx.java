package com.epam.learn.l21;

import java.util.Arrays;

public class MergeSortEx {

    //n*log(n)

    public static void main(String[] args) {
        int[] arr = new int[]{1, 23, 4, 3, 12, 5};
        System.out.println(Arrays.toString(MergeSortEx.sort(arr)));

    }

    private static int[] sort(int[] data) {
        if (data == null) return null;
        if (data.length < 2) return data;

        int[] left = new int[data.length / 2];
        System.arraycopy(data, 0, left, 0, data.length / 2);

        int[] right = new int[data.length - data.length / 2];
        System.arraycopy(data, data.length / 2, right, 0, data.length - data.length / 2);

        left = sort(left);
        right = sort(right);


        return mergeArr(left, right);
    }

    private static int[] mergeArr(int[] left, int[] right) {
        int[] data = new int[left.length + right.length];
        int posL = 0;
        int posR = 0;
        for (int i = 0; i < data.length; i++) {
            if (posL == left.length) {
                data[i] = right[posR];
                posR++;
            } else if (posR == right.length) {
                data[i] = left[posL];
                posL++;
            } else if (left[posL] < right[posR]) {
                data[i] = left[posL];
                posL++;
            } else {
                data[i] = right[posR];
                posR++;
            }
        }
        return data;
    }
}
