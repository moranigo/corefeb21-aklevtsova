package com.epam.learn.l21;

public class Main {

    private static final int[] arr = new int[]{1, 3, 23, 154, 244};

    public static void main(String[] args) {
        // O
        //O(1) - константная сложность
        //O(log(n)) - логарифмическая сложность
        //О(n) - линейная сложность
        //O(n*log(n)) - квазилинейная сложность
        //O(n^2) - квадратичная сложность

        int i = 23;

        boolean isInArray = false;
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] == i) {
                isInArray = true;
                break;
            }
        }
        System.out.println(isInArray);
    }
}
