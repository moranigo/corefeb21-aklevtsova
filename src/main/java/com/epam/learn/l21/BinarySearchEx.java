package com.epam.learn.l21;

public class BinarySearchEx {

    public static void main(String[] args) {
        int[] arr = new int[]{1, 3, 23, 154, 244};
        BinarySearchEx binarySearchEx = new BinarySearchEx();
        binarySearchEx.binarySearch(arr, 0, arr.length, 3);

    }

    void binarySearch(int[] array, int first, int last, int item) {
        int position;
        int comparisonCount = 1;

        position = (first + last) / 2;
        while (array[position] != item && first <= last) {
            comparisonCount++;
            if (array[position] > item) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = (first + last) / 2;
        }
        if (first <= last) {
            System.out.println(item + " is " + position + "  in array");
            System.out.println("found im array " + comparisonCount);
        } else {
            System.out.println("Element not in array");
        }
    }
}
