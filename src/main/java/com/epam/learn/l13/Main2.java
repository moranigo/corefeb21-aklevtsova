package com.epam.learn.l13;

import javax.sound.midi.Soundbank;
import java.util.*;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Barsik", "Murzik", "Snegok");
        List<String> list2 = Arrays.asList("Pushok", "Barsik", "Murzik", "Snegok");
        List<String> list3 = Arrays.asList("Barsik", "Murzik", "Snegok", "Pushok", "Rygik");

        System.out.println(Collections.indexOfSubList(list2, list));
        System.out.println(Collections.lastIndexOfSubList(list3, list));


        System.out.println("Befoew swap \n" + list);
        Collections.swap(list2, 0, 2);
        System.out.println(list2);

        Collection<String> collection = Collections.unmodifiableCollection(list2);
//        collection.remove("Barsik")
//        list2.remove("Barsik");
//        System.out.println(list2);

        Collection<String> collection1 = Collections.synchronizedCollection(list2);

        List list4 = new ArrayList();
        list4.add("String");
        list4.add(5);
        Collection<String> collection2 = Collections.checkedCollection(list4, String.class);
        System.out.println(collection2);

        Set<String> singletonSet = Collections.singleton("Barsik");
        System.out.println(singletonSet);
        //singletonSet.add("asd");

        Collection<String> collection3 = Collections.nCopies(4, "Barsik");
        System.out.println(collection3);

        System.out.println(Collections.frequency(collection1, "Barsik"));

        Integer[] array = new Integer[]{2, 4, 1, 3};
        Comparator<Integer> comparator = Collections.reverseOrder();
        Arrays.sort(array, comparator);
        System.out.println(Arrays.asList(array));

        Vector<String> vector = new Vector<>();
        vector.add("Barsik");
        vector.add("Murzik");
        Enumeration<String> enumeration = vector.elements();
        List<String> list1 = Collections.list(enumeration);
        System.out.println(list1);

        List<String> list5 = new ArrayList<>();
        list5.addAll(vector);
        System.out.println(list5);

        List<String> list6 = new ArrayList<>();
        list6.add("Sharik");
        list6.add("Bobik");
        boolean isUnique = Collections.disjoint(list1, list6);
        System.out.println(isUnique);


        List<String> list7 = new ArrayList<>();
        list7.add("Sharik");
        list7.add("Reks");
        boolean isUnique2 = Collections.disjoint(list7, list6);
        System.out.println(isUnique2);


        List<Integer> numList = new ArrayList<>();
        Collections.addAll(numList, array);
        System.out.println(numList);

        Map<String, Boolean> map = new HashMap<>();
       // map.put("Barsik", true);
        System.out.println(Collections.newSetFromMap(map));

        Deque<String> deque = new LinkedList<>();
        Queue<String> queue = Collections.asLifoQueue(deque);


    }
}
