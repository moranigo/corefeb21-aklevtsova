package com.epam.learn.l13;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(2);
        list.add(4);

        //merge sort O(n*logn)
        Collections.sort(list);
        System.out.println(list);

        //log(n)
        System.out.println(Collections.binarySearch(list,3));

        Collections.reverse(list);
        System.out.println(list);

        Collections.shuffle(list);
        System.out.println(list);

        List<Integer> list2 = new ArrayList<>(30);
        System.out.println(list2);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        list2.add(1);
        Collections.fill(list2,3);
        System.out.println(list2);

        List<String> list3 = Arrays.asList("Barsik","Murzik", "Snegok");
        System.out.println("Index of Murzik: " + Collections.binarySearch(list3, "Murzik"));
        System.out.println("Index from index.of "+list3.indexOf("Murzik"));
        Collections.fill(list3,"Barsik");
        System.out.println(list3);

        Collection<String> collection  = Arrays.asList("red", "green", "blue");
        System.out.println("Max: "+Collections.max(collection));
        System.out.println("Min: "+Collections.min(collection));


        System.out.println("---------");
        System.out.println(list);
        Collections.copy(list, list2);
        System.out.println(list);

        list.addAll(list2);
        System.out.println(list);

        System.out.println("---------");
        List<String> list4 = Arrays.asList("Barsik","Murzik", "Snegok");
        Collections.rotate(list4, 1);
        System.out.println(list4);

        Collections.replaceAll(list4, "Murzik", "Joye");
        System.out.println(list4);



    }
}
