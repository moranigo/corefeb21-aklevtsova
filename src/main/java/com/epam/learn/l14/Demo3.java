package com.epam.learn.l14;

public class Demo3 {
    public static void main(String[] args) {
        Flyable flyable  = new Flyable() {
            @Override
            public void fly() {
                System.out.println(
                        "I am flying..."
                );
            }
        };
        System.out.println(flyable.getClass());
        Bird bird = new Bird();
        System.out.println(bird.getClass());

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        System.out.println(thread.isAlive());
        System.out.println(thread.getState());
    }
}
