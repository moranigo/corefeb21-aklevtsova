package com.epam.learn.l14;

public class DemoJoin {
    public static void main(String[] args) {
        NewThread obj1 = new NewThread("One");
        NewThread obj2 = new NewThread("Two");
        NewThread obj3 = new NewThread("Three");

        System.out.println("Thread One is alive " + obj1.t.isAlive());
        System.out.println("Thread Two is alive " + obj2.t.isAlive());
        System.out.println("Thread Three is alive " + obj3.t.isAlive());

        try {
            System.out.println("Waiting for threads to finish");
            obj1.t.join();
            obj2.t.join();
            obj3.t.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread One is alive " + obj1.t.isAlive());
        System.out.println("Thread Two is alive " + obj2.t.isAlive());
        System.out.println("Thread Three is alive " + obj3.t.isAlive());
        System.out.println("Thread Main existing");

    }
}
