package com.epam.learn.l4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Operators {

    private static final boolean isTrue = true;

    //if
    public static void main(String[] args) {

        Colours colours = Colours.RED;
        //Этот пример плох, потому что при добавлении в enum ещё одного цвета, будет работать плохо
        if(getColorByTime(colours) ==Colours.GREEN) {
            System.out.println("GREEN");
        } else if(getColorByTime(colours) ==Colours.YELLOW) {
            System.out.println("YELLOW");
        } else {
            System.out.println("RED");
        }

        if(getColorByTime(colours) ==Colours.GREEN) {
            System.out.println("GREEN");
        } else if(getColorByTime(colours) ==Colours.YELLOW) {
            System.out.println("YELLOW");
        } else if (getColorByTime(colours) ==Colours.RED){
            System.out.println("RED");
        } else {
            throw new RuntimeException();
        }
    }

    private void ifExample() {
        //reader  - позволяет считывать с консоли
        String line = null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            //считывание линии с консоли
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int finalResult = Integer.parseInt(Objects.requireNonNull(line));

        //в таком, случее если будет 4 - то будет все равно красный
        //использовать блок else осторожно, только будучи уверенным, что это для всего остального.
        // иначе лучше использовать else if
        if (finalResult == 1) {
            System.out.println("green");
        } else if (finalResult == 2) {
            System.out.println("yellow");
        } else {
            System.out.println("red");
        }
    }

    private static Colours getColorByTime (Colours colours) {
        return colours;
    }
}
