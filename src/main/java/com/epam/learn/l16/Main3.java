package com.epam.learn.l16;

import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipInputStream;

public class Main3 {
    public static void main(String[] args) {
        //types - text, binary
        //ways - input/output
        //type of I/O - Memory / Pipe/ File / Object Serialization

        //text
        Reader reader;
        Writer writer;

        //binary
        InputStream inputStream;
        OutputStream outputStream;
    }

    void getReaderReal() {
        PipedReader pipedReader; //Pipe
        CharArrayReader charArrayReader; //Memory
        StringReader stringReader;  //Memory
        BufferedReader bufferedReader;  //Memory
        FilterReader filterReader; //File
        InputStreamReader inputStreamReader;
    }

    void getWriterReal() {
        PipedWriter pipedWriter; //Pipe
        CharArrayWriter charArrayWriter;  //Memory
        StringWriter stringWriter;
        BufferedWriter bufferedWriter;
        PrintWriter printWriter;
        OutputStreamWriter outputStreamWriter;
        FileWriter fileWriter; //File
    }

    void getInputStreamReal() {
        ByteArrayInputStream byteArrayInputStream;  //Memory
        FileInputStream fileInputStream; //File
        SequenceInputStream sequenceInputStream;
        ObjectInputStream objectInputStream; //Object
        FilterInputStream filterInputStream;
        PipedInputStream pipedInputStream; //Pipe
        BufferedInputStream bufferedInputStream; //Memory
        DataInputStream dataInputStream;
        PushbackInputStream pushbackInputStream;
        ZipInputStream zipInputStream;
    }

    void getOutputStreamReal() {
        ByteArrayOutputStream byteArrayOutputStream;  //Memory
        FileOutputStream fileOutputStream; //File
        PipedOutputStream pipedOutputStream; //Pipe
        ObjectOutputStream objectOutputStream; //Object
        FilterOutputStream filterOutputStream;
        PrintStream printStream;
    }

}
