package com.epam.learn.l16;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main5 {
    public static void main(String[] args) throws IOException {
        byte[] bytesToWrite = {1,2,3};
        byte[] bytesReaded = new byte[10];
        String fileName = "Main5.txt";

        FileOutputStream outFile = null;
        FileInputStream inFile = null;

        outFile = new FileOutputStream(fileName);
        System.out.println("File is opened for write");
        outFile.write(bytesToWrite);
        System.out.println("Wrote " + bytesToWrite.length + " bytes");
        outFile.close();
        System.out.println("OutStream closed");

        inFile = new FileInputStream(fileName);
        System.out.println("File is opened for read");
        int bytesAvailable = inFile.available();
        System.out.println("Ready to read " + bytesAvailable);

        int count = inFile.read(bytesReaded, 0, bytesAvailable);
        System.out.println("Read " + count+ " bytes");
        inFile.close();
        System.out.println("InStream close");

    }
}
