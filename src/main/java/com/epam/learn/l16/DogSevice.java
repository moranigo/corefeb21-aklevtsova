package com.epam.learn.l16;

public class DogSevice {

    public int getDogCount(boolean isTrue) {
        if (isTrue) {
            return 2;
        } else {
            return 1;
        }
    }
}
