package com.epam.learn.l16;

import java.io.*;

public class Main4 {
    public static void main(String[] args) {
        //InputStream | OutputStream
        //абстрактный класс для описания поточного ввода
        InputStream inputStream;
        //абстрактныйкласс который описывает поточный вывод
        OutputStream outputStream;

        // для буфферизации ввода
        BufferedInputStream bufferedInputStream;

        //для буферизации вывода
        BufferedOutputStream bufferedOutputStream;

        //поток, читающий из массива байт
        ByteArrayInputStream byteArrayInputStream;

        //поток, пишущий в массива байт
        ByteArrayOutputStream byteArrayOutputStream;

        //поток ввода, который содержит метода для чтения данных стандартных типово java
        DataInputStream dataInputStream;

        //для записи данных стандартных типов java
        DataOutputStream dataOutputStream;


        //читатет байты из файла
        FileInputStream fileInputStream;

        //пишет байты в файл
        FileOutputStream fileOutputStream;

        //реализует шаблон адаптера
        FilterInputStream filterInputStream;
        FilterOutputStream filterOutputStream;

        //подсчитывает сколько строк было считано. Устаревший
        LineNumberInputStream lineNumberInputStream;

        //для сериализации объектов
        ObjectInputStream objectInputStream;
        //для десериализации объектов
        ObjectOutputStream objectOutputStream;

        //поток читающий данные из канала
        PipedInputStream pipedInputStream;
        //поток, пишущий данные в канал
        PipedOutputStream pipedOutputStream;

        //дляконвертации строк в байтовый     поток
        PrintStream printStream;
        //фильтр позволяющий вернуть во входный поток считанные из него данные
        PushbackInputStream pushbackInputStream;
        //считывает данные из других двух и более других потоков
        SequenceInputStream sequenceInputStream;

        //read



    }
}
