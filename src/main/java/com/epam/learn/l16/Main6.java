package com.epam.learn.l16;

import java.io.*;

public class Main6 {
    public static void main(String[] args) throws IOException {
        PipedOutputStream pipedOutputStream;
        PipedInputStream pipedInputStream;

        int countRead = 0;
        int[] toRead = null;

        pipedInputStream = new PipedInputStream();
        pipedOutputStream = new PipedOutputStream(pipedInputStream);
        for (int i = 0; i < 20; i++) {
            pipedOutputStream.write(i);
        }

        int willRead = pipedInputStream.available();
        toRead = new int[willRead];
        for (int i = 0; i < willRead; i++) {
            toRead[i] = pipedInputStream.read();
            System.out.print(toRead[i]+ " ");
        }

        FileInputStream inFile2;
        FileInputStream inFile3;

        SequenceInputStream sequenceInputStream;
        FileOutputStream outFile;

        inFile2 = new FileInputStream("File2.txt");
        outFile = new FileOutputStream("File2.txt");
        outFile.write("I am file 2.".getBytes());
        outFile.flush();

        inFile3 = new FileInputStream("File3.txt");
        outFile = new FileOutputStream("File3.txt");
        outFile.write("I am file 3.".getBytes());
        outFile.flush();


        sequenceInputStream = new SequenceInputStream(inFile2,inFile3);
        outFile = new FileOutputStream("File4.txt");
        int readByte = sequenceInputStream.read();
        while(readByte !=-1) {
            outFile.write(readByte);
            readByte = sequenceInputStream.read();
        }


    }
}
