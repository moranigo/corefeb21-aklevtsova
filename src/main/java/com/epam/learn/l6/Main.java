package com.epam.learn.l6;

public class Main {

    public static void main(String[] args) {
        StringBuilder builder;
        StringBuffer buffer;

        String name = "Va" + "Si" + "Li" + "SK";
        builder= new StringBuilder("barsik");

        System.out.println(builder.length()); //не показывает доп 16 символов

        //вывод символа по позиции
        System.out.println(builder.charAt(2));

        //замена символа
        builder.setCharAt(2, 'R');
        System.out.println(builder);

        //вернуть новую подстроку
        System.out.println(builder.subSequence(2,4));

        //StringBuffer - невозможно сравнить 2 объекта и хеш коды вычислиются как для класса object

        //добавляет строку в конец
        builder.append(" the").append(" cat");
        System.out.println(builder);

        //вставить строку в середина
        builder.insert(6, " is");
        System.out.println(builder);

        //перевернуть строку
        builder.reverse();

    }

    private String getInfo() {
        //не имеет смысла - в  return автоматически это сделает, даже если написать
        //"at"+"bat" - это будет одинобъект.Если в ретурне делается объединение строк, не нужно делать
        //стринг Билдер, это сделает автоматически. В остальных местах лучше так, ток если 2-3 строки, тогда можно принебречь
        return String.valueOf(new StringBuilder("at" + "bat"));
    }
}
