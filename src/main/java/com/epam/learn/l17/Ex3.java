package com.epam.learn.l17;

import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        String instr = "NAme Joye Age: 28 Id: 77";
        Scanner scanner = new Scanner(instr);
        scanner.findInLine("Age:");
        if (scanner.hasNext()) {
            System.out.println(scanner.next());
        } else {
            System.out.println("Error");
        }

        scanner.close();
    }
}
