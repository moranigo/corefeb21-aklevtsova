package com.epam.learn.l5.loop;

import com.epam.learn.l5.Animal;
import com.epam.learn.l5.Cat;
import com.epam.learn.l5.Dog;

public class ForEachLoopExample {
    public static void main(String[] args) {
        Cat barsik = new Cat("barsik", 2);
        Cat murzik = new Cat("murzik", 1);
        Cat pushok = new Cat("pushok", 3);
        Dog sharik = new Dog("sharik",7);

       // Cat[] cats = new Cat[]{barsik, murzik, pushok};
        Animal[] animals = new Animal[]{barsik,murzik, pushok};

        for(Animal animal: animals) {
            Cat cat = (Cat) animal; // Если сюда попадет шарик, то будет ClassCastException
            System.out.println(animal.toString());
        }
    }
}
