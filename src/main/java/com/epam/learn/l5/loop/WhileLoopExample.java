package com.epam.learn.l5.loop;

public class WhileLoopExample {

    public static void main(String[] args) {
        int i=0;

        //break - прерывание цикла
        //continue - прерывание продолжение и запускаем следующую итерацию цикла
        while (true) {
            i++;

            if (i==50) {
                continue;
            }
            System.out.println("current i is: "+ i);

            if(i==100) {
                break;
            }
        }
    }
}
