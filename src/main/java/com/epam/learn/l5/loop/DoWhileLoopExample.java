package com.epam.learn.l5.loop;

public class DoWhileLoopExample {
    public static void main(String[] args) {
        boolean isTrue = false;

        do {
            System.out.println("Hello world!");
        } while (isTrue);
    }
}
