package com.epam.learn.l5.loop;

public class ForLoopExample {
    public static void main(String[] args) {

        //exp1 - init; exp2 - condition; rxp
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }

        int i = 5;
        for (; i < 10; ) {
            System.out.println(i++);
        }
    }
}
