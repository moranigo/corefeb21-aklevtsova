package com.epam.learn.l5.array;

import java.util.Arrays;

public class ArraysExample {
    public static void main(String[] args) {
        int i = 5;
        int i2 = 6;
        int i3 = 7;
        int[] array = new int[5];
        //Те значения, что не будут заданы - будут проинициализированы дефолтным значением,
        // определяемого типом данным в массиве. Для int - 0; для Integer -  null
        //  array = new int[10];

        array[0] = i;
        array[1] = i2;
        array[2] = i3;


        System.out.println(array); //так выводится адрес
        System.out.println(Arrays.toString(array));

        int[] array2 = new int[]{5, 11, 2};
        System.out.println(Arrays.toString(array2));

        int[] array3 = {1,2,3};
        System.out.println(Arrays.toString(array3));

        System.out.println(Runtime.getRuntime().totalMemory());
        System.out.println(Runtime.getRuntime().freeMemory());
    }
}
