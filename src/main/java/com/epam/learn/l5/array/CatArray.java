package com.epam.learn.l5.array;

import com.epam.learn.l5.Cat;

import java.util.Arrays;

public class CatArray {
    public static void main(String[] args) {

        Cat barsik = new Cat("barsik", 10);
        Cat murzik = new Cat("murzik",4);

        Cat[] catsArray = new Cat[2];
        catsArray[0] = barsik;
        catsArray[1] = murzik;

        Cat[] cats = new Cat[] {barsik,murzik};
        System.out.println(Arrays.toString(cats));
    }
}
