package com.epam.learn.l5.array;

import java.util.Arrays;

public class MultiArrayExample {
    public final int someField = 5;

    public static void main(String[] args) {
        //Как сделать многомерные массивы. 1 вариант
        int[][] array = new int[4][3];
        int count = 4;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                array[i][j] = ++count;
            }
        }
        System.out.println(Arrays.deepToString(array));

        MultiArrayExample multiArrayExample = new MultiArrayExample();
        //multiArrayExample.somefield = 6; - нельзя так как поле final
        System.out.println(multiArrayExample.someField);

        //Как сделать многомерные массивы. 2 вариант
        int[][] arr = new int [2][];
        arr[0] = new int[2];
        arr[0][0]=2;
        arr[1] = new int[5];
        System.out.println(Arrays.deepToString(arr));

    }
}
