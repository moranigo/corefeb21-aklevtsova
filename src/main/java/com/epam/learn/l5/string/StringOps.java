package com.epam.learn.l5.string;

public class StringOps {
    public static void main(String[] args) {
        String ex = "bar";
        String ex2 = "cat";
        System.out.println(ex.concat(ex2));


        String s1 = "bar";
        String s2 = "b"+"ar";
        String s3 = new String("bar").intern();
        System.out.println(ex=="bar");
        System.out.println(ex==s1);
        System.out.println(ex==s2);
        System.out.println(ex==s3);
    }
}
