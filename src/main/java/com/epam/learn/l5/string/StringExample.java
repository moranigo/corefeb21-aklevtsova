package com.epam.learn.l5.string;

import java.io.UnsupportedEncodingException;

public class StringExample {

    public static void main(String[] args) throws UnsupportedEncodingException {
        //immutable - неизменеямый.
        String example = "this is a string";
        String emptyString = "";
        String nullString = null;

        char[] charArray = example.toCharArray();
        String stringFromChar = new String(charArray, 2, 3);
        System.out.println(stringFromChar);

        byte[] ascii = {65,66,67,68,69,70};
        System.out.println(new String(ascii));

        byte[] data = {(byte) 0xE3,(byte) 0xEE};
        System.out.println(new String(data, "CP1251"));
    }
}
