package com.epam.learn.l5.string;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StringPool {


    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String param = reader.readLine();
            if (param.equals("1")) {
                new StringPool().fromLiteral();

            } else {
                new StringPool().fromLiteral2();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fromLiteral() {
        String name = "barsik";
        System.out.println(name=="barsik");
    }

    private void fromLiteral2() {
        String name = "murz";
        System.out.println(name =="murz");
    }
}

