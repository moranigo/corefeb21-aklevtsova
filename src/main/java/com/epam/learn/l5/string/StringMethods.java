package com.epam.learn.l5.string;

import java.util.Arrays;

public class StringMethods {
    public static void main(String[] args) {
        String name = "Barsik";

        System.out.println(name.charAt(2));
        System.out.println(name.length());
        System.out.println(name.subSequence(2,5));

        char[] charArr = new char[2];
        name.getChars(2,4,charArr,0);
        System.out.println(Arrays.toString(charArr));

        String empty = "";
        String whitespace = "     ";
        String nullStr = null;
        System.out.println(empty.isEmpty());
       // System.out.println(nullStr.isEmpty());  - NullPointerException
        System.out.println(name.isEmpty());

        System.out.println(name.codePointAt(1)); //отдает код буквы по индексу
        System.out.println(name.codePointBefore(1));
        System.out.println(Arrays.toString(name.getBytes())); //отдает по байтам

        System.out.println(whitespace.isBlank());
        System.out.println(whitespace.isEmpty());

        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());

        System.out.println(name.indexOf("r"));
        String value = "Abrakadabra";
        System.out.println(value.indexOf("a", 6));
        System.out.println(value.indexOf(97));
        System.out.println(value.lastIndexOf(97));
        System.out.println(value.lastIndexOf("abr"));

        System.out.println(value.replace("a", ""));
        System.out.println(value.replaceFirst("a", ""));
        String value2 = "Abrakadabra";
        System.out.println(value2.replaceAll("a", ""));

        System.out.println(value.contains("ak"));
        System.out.println(value.startsWith("Ab"));
        System.out.println(value.endsWith("Ab"));

        System.out.println("  a ss  ".trim());//a ss
        System.out.println(name.substring(2));
        System.out.println(name.substring(2,4));

        System.out.println(Arrays.toString("barsik;murzik;pishok".split(";")));
    }
}
