package com.epam.learn.l3;

public class LogicOp {

    //logic: &&, ||, <, >, ==, !=, >=, <=
    //default  = false
    private final boolean paramA = true;
    private final boolean paramB = false;
    private final boolean paramC = true;

    public static void main(String[] args) {
        LogicOp logicOp = new LogicOp();
        System.out.println("1: " + logicOp.checkResultFirst(logicOp.paramA, logicOp.paramB, logicOp.paramC));
        System.out.println("2: " + logicOp.checkResultSecond(logicOp.paramA, logicOp.paramB));
        System.out.println("3: " + logicOp.checkResultThird(logicOp.paramB, logicOp.paramC));
        System.out.println("4: " + logicOp.checkResultFourth(logicOp.paramA, logicOp.paramB, logicOp.paramC));
    }

    private boolean checkResultFirst(boolean a, boolean b, boolean c) {
        return a && b || c;
    }

    private boolean checkResultSecond(boolean a, boolean b) {
        return a && b;
    }

    private boolean checkResultThird(boolean b, boolean c) {
        return c || b;
    }

    private boolean checkResultFourth(boolean a, boolean b, boolean c) {
        return a || b && c;
    }
}
