package com.epam.learn.l3;

public class Initial {

    private static int stCount = 3;

    static {
        stCount = 2;
    }

    private int count = 5;

    {
        count = 10;
    }

    public Initial(int count, int stCount) {
        this.count = count;
        Initial.stCount = stCount;
    }

    public static void main(String[] args) {
        Initial initial = new Initial(100, 102);
        System.out.println(initial.count);
        System.out.println(stCount);
    }
}
