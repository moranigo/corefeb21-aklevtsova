package com.epam.learn.l3;

public class ArithmeticsOp {

    //Arithmetics: +, += ,- ,-= , %, %=, *, *= , /, /=, ++, --
    public static void main(String[] args) {
        //-=
        int result = 6;
        result -= 2;
        System.out.println(result);

        int result2 = 6;
        result2 = result2 - 2;
        System.out.println(result2);

        //result3
        int result3 = 2;
        result3 *= 3;
        System.out.println(result3);

        //%
        ArithmeticsOp main = new ArithmeticsOp();
        System.out.println(main.isEven(2));
        System.out.println(main.isOdd(5));

        //++
        int param1 = 10;
        int param2 = 10;
        //++ after number - return value, then + 1
        System.out.println(param1++); // 10
        //++ before nubmer = +1, then return value;
        System.out.println(++param2); // 11

    }

    private boolean isEven(int param) {
        return param % 2 == 0;
    }

    private boolean isOdd(int param) {
        return param % 2 != 0;
    }
}
