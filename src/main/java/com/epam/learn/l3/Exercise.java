package com.epam.learn.l3;

public class Exercise {

    private final boolean paramA = true;
    private final boolean paramB = false;
    private final boolean paramC = true;

    public static void main(String[] args) {
        Exercise ex = new Exercise();
        System.out.println("2: " + ex.checkResultSecond(ex.paramA, ex.paramB));
        System.out.println("3: " + ex.checkResultThird(ex.paramB, ex.paramC));
        System.out.println("4: " + ex.checkResultFourth(ex.paramA, ex.paramB, ex.paramC));

    }

    private boolean checkResultSecond(boolean a, boolean b) {
        return a && b;
    }

    private boolean checkResultThird(boolean b, boolean c) {
        return c || b;
    }

    private boolean checkResultFourth(boolean a, boolean b, boolean c) {
        return a || b && c;
    }
}
