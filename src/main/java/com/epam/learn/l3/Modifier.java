package com.epam.learn.l3;

class Modifier {

    /*
    for classes:
     - public
     - default
     - abstract (для создания абстрактных методов)
     */

    /*
    for methods:
     - public
     - private
     - protected
     - default
     - static
     - final
     - native
     - synchronized
     - strictfp (указывает какие инструкции должны применятеся к процессу в разных ОС.Связано с точностьб вычислений)
    */

     /*
    for var:
     - public
     - private
     - protected
     - default
     - static
     - final
     - volatile (значение одно для всех потоков)
     - transient (игнорируется при сериализации)
      */
}
