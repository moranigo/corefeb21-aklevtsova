package com.epam.learn.l3;

public class Medusa extends Animal {

    public static void main(String[] args) {
        Animal animal = new Medusa();
        animal.eat();
    }

    @Override
    public void eat() {
        System.out.println("Medusa ia eating!");
    }
}
