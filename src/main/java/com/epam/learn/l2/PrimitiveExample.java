package com.epam.learn.l2;

public class PrimitiveExample {
    //целые
    byte someByte;
    short someShort;
    int number = 1;
    long bigNumber;

    //дробные
    float nFloat;
    double nDouble;

    //символьные
    char someChar;

    //логический
    boolean isTrue;

}
