package com.epam.learn.l2;

public class DefaultValue {

    // (byte) 0
    byte dByte;
    //0
    short dShort;
    //0
    int dInt;
    //0
    long dLong;

    //0.0
    float dFloat;
    //0.0
    double dDouble;

    //false
    boolean dBoolean;

    //'\u0000' (null)
    char dChar;

    public static void main(String[] args) {
        System.out.println(new DefaultValue().dChar);
    }
}
