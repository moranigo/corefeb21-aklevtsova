package com.epam.learn.l2;

public class TypeCasting {
    public static void main(String[] args) {
        double v1 = 2.00;
        float f1 = 2.0F;
        float f2 = (float) v1; //понижающее явное приведение типов
        double v2 = f2; //повышающее неявное приведение типов

        System.out.println(f2);

        System.out.println(f1 == f2);
        System.out.println(v1 == v2);

        float v = 1/2.3F;
        int v3 = (int) (1/2.3F); //потеря точности
    }
}
