package com.epam.learn.l2;

public class Wrapper {

    Byte aByte;
    Short aShort;
    Integer aInt;
    Long aLong;

    Float aFloat;
    Double aDouble;

    Boolean aBoolean;
    Character character;

    public static void main(String[] args) {
        int a = 5;
        System.out.println(a);

        System.out.println(Integer.valueOf("5") + 2);
        System.out.println("5" + 2);

        System.out.println(Integer.reverse(521));
        System.out.println(new StringBuilder("hi").reverse().toString());
        System.out.println(new Wrapper().character);
    }
}