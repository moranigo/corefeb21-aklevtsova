package com.epam.learn.l16;


import org.junit.Assert;
import org.junit.Test;

public class DogServiceTest {

    @Test(expected = RuntimeException.class)
    public void expectRuntimeExceptionTest () {
        throw new RuntimeException();
    }

    @Test
    public void testGetDogCount() {
        int expectedDogCountWithTrue = 2;
        int expectedDogCountWithFalse = 1;
        DogSevice dogSevice = new DogSevice();
        Assert.assertEquals(dogSevice.getDogCount(true), expectedDogCountWithTrue);
        Assert.assertEquals(dogSevice.getDogCount(false), expectedDogCountWithFalse);
    }
}
