package com.epam.learn.l16;



import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;


public class ByteArrayStreamsTest {

    @Test
    public void test() {

        byte[] bytes = {1,-1,0};
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        assertEquals(in.read(), 1);

        int read = in.read();
        assertEquals(read, 255);
        assertEquals((byte) read, (byte) -1);
        assertEquals(in.read(),0);
    }

    @Test
    public void test2() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(10);
        out.write(11);

        byte[] bytes = out.toByteArray();
        assertEquals(bytes[0], (byte) 10);
        assertEquals(bytes[1], (byte) 11);
    }


}
